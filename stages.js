// Based on http://steelchallenge.com/wp-content/uploads/2011/07/SCSARules.pdf

var Stages = [
  {
    name: "SC-101. 5 to Go",
    boxes: [
      {
        x: 0, y: 0, width: 3, height: 3
      }
    ],
    plates: [
      {
        size: "10",
        height: 5,
        x: 10.25,
        y: 54
      },
      {
        size: "10",
        height: 5,
        x: 3.5,
        y: 45
      },
      {
        size: "10",
        height: 5,
        x: -3.5,
        y: 36
      },
      {
        size: "10",
        height: 5,
        x: -10.25,
        y: 30
      },
      {
        size: "12",
        height: 5,
        x: 17 + 1 / 12,
        y: 21
      },
    ]
  },

  {
    name: "SC-102. Showdown",
    boxes: [
      {
        x: -4.5, y: 0, width: 3, height: 3
      },
      {
        x: 4.5, y: 0, width: 3, height: 3
      }
    ],
    plates: [
      {
        size: "18x24",
        height: 5.5,
        x: -9,
        y: 75
      },
      {
        size: "18x24",
        height: 5.5,
        x: 9,
        y: 75
      },
      {
        size: "10",
        height: 5,
        x: -4,
        y: 30
      },
      {
        size: "10",
        height: 5,
        x: 4,
        y: 30
      },
      {
        size: "12",
        height: 5,
        x: 0,
        y: 36
      },
    ]
  },

  {
    name: "SC-103. Smoke & Hope",
    boxes: [
      {
        x: 0, y: 0, width: 3, height: 3
      }
    ],
    plates: [
      {
        size: "18x24",
        height: 5.5,
        x: -14,
        y: 21
      },
      {
        size: "18x24",
        height: 5.5,
        x: 14,
        y: 21
      },
      {
        size: "18x24",
        height: 5.5,
        x: -9,
        y: 27
      },
      {
        size: "18x24",
        height: 5.5,
        x: 9,
        y: 27
      },
      {
        size: "12",
        height: 5,
        x: 0,
        y: 42
      },
    ]
  },

  {
    name: "SC-104. Outer Limits",
    boxes: [
      {
        x: -6, y: 0, width: 4, height: 4
      },
      {
        x: 0, y: 0, width: 4, height: 4
      },
      {
        x: 6, y: 0, width: 4, height: 4
      }
    ],
    plates: [
      {
        size: "18x24",
        height: 5.5,
        x: -6,
        y: 105
      },
      {
        size: "18x24",
        height: 5.5,
        x: 6,
        y: 105
      },
      {
        size: "12",
        height: 5,
        x: -12,
        y: 60
      },
      {
        size: "12",
        height: 5,
        x: 12,
        y: 60
      },
      {
        size: "12",
        height: 5,
        x: 0,
        y: 54
      },
    ]
  },

  {
    name: "SC-105. Accelerator",
    boxes: [
      {
        x: 0, y: 0, width: 3, height: 3
      }
    ],
    plates: [
      {
        size: "10",
        height: 5,
        x: -12,
        y: 30
      },
      {
        size: "18x24",
        height: 5.5,
        x: -4,
        y: 30
      },
      {
        size: "12",
        height: 5,
        x: 6,
        y: 60
      },
      {
        size: "18x24",
        height: 5.5,
        x: 20,
        y: 60
      },
      {
        size: "12",
        height: 5,
        x: 0,
        y: 45
      },
    ]
  },

  {
    name: "SC-106. Pendulum",
    boxes: [
      {
        x: 0, y: 0, width: 3, height: 3
      }
    ],
    plates: [
      {
        size: "10",
        height: 5,
        x: -6,
        y: 54
      },
      {
        size: "10",
        height: 5,
        x: 6,
        y: 54
      },
      {
        size: "12",
        height: 6,
        x: -12,
        y: 54
      },
      {
        size: "12",
        height: 6,
        x: 12,
        y: 54
      },
      {
        size: "12",
        height: 5,
        x: 0,
        y: 30
      },
    ]
  },

  {
    name: "SC-107. Speed Option",
    boxes: [
      {
        x: 0, y: 0, width: 3, height: 3
      }
    ],
    plates: [
      {
        size: "10",
        height: 5,
        x: -12,
        y: 30
      },
      {
        size: "10",
        height: 5,
        x: -6,
        y: 60
      },
      {
        size: "10",
        height: 5,
        x: 6.5,
        y: 24
      },
      {
        size: "10",
        height: 5,
        x: 21,
        y: 45
      },
      {
        size: "18x24",
        height: 5.5,
        x: -21.5,
        y: 105
      },
    ]
  },

  {
    name: "SC-108. Roundabout",
    boxes: [
      {
        x: 0, y: 0, width: 3, height: 3
      }
    ],
    plates: [
      {
        size: "12",
        height: 5,
        x: -9,
        y: 45
      },
      {
        size: "12",
        height: 5,
        x: 8,
        y: 45
      },
      {
        size: "12",
        height: 5,
        x: -2,
        y: 21
      },
      {
        size: "12",
        height: 5,
        x: 8,
        y: 21
      },
      {
        size: "12",
        height: 5,
        x: 2,
        y: 30
      },
    ]
  },
];