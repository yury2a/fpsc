var INCH2MM = 25.4;
var FT2INCH = 12;
var FT2MM = FT2INCH * INCH2MM;
var POLE_WIDTH = 1.4 * INCH2MM;

function line(doc, x1, y1, x2, y2) {
  doc.lines([[x2 - x1, y2 - y1]], x1, y1, [1, 1], 'S');
}

function toFeet(n) {
  return Math.floor(n) + "'" + Math.floor((n - Math.floor(n)) * 12) + "\"";
}

function getRotatedR(x, y, r) {
  var d = Math.sqrt(x * x + y * y), dx = x / d, dy = y / d;
  var x1 = x - dy * r, y1 = y + dx * r;
  var x0 = y * x1 / y1;
  return Math.abs(x - x0);
}

function createDoc(options) {
  var data = options.data;
  var page = options.page;
  var box = data.boxes[options.box];
  var figures = [];
  for (var i = 0; i < 5; i++) {
    var plate = data.plates[i];
    var scale = (plate.y - box.y) / options.distance;
    var x = (plate.x - box.x) * FT2MM / scale;
    var y = (options.height - plate.height) * FT2MM / scale;
    var figure = {
      x: x,
      y: y,
      poleh: plate.height * FT2MM / scale,
      polew: POLE_WIDTH / scale,
      stop: i == 4,
    };
    switch (plate.size) {
      case "10":
        figure.type = "circle";
        var rh = 5 * INCH2MM / scale;
        var rw = rh;
        if (options.rotated) {
          rw = getRotatedR((plate.x - box.x) * FT2INCH, 
                           (plate.y - box.y) * FT2INCH, 5) * INCH2MM / scale;
        }
        figure.rw = rw;
        figure.rh = rh;
        figure.bounds = [x - rw, y, x + rw, y + 2 * rh];
        break;
      case "12":
        figure.type = "circle";
        var rh = 6 * INCH2MM / scale;
        var rw = rh;
        if (options.rotated) {
          rw = getRotatedR((plate.x - box.x) * FT2INCH, 
                           (plate.y - box.y) * FT2INCH, 6) * INCH2MM / scale;
        }
        figure.rw = rw;
        figure.rh = rh;
        figure.bounds = [x - rw, y, x + rw, y + 2 * rh];
        break;
      case "18x24":
        figure.type = "rect";
        var w = 18 * INCH2MM / scale;
        var h = 24 * INCH2MM / scale;
        figure.w = w;
        figure.h = h;
        figure.bounds = [x - w / 2, y, x + w / 2, y + h];
        break;
    }
    figures.push(figure);
  }

  var minX = Infinity, minY = Infinity, maxX = -Infinity, maxY = -Infinity;
  figures.forEach(function (f) {
    minX = Math.min(minX, f.bounds[0]);
    minY = Math.min(minY, f.bounds[1]);
    maxX = Math.max(maxX, f.bounds[2]);
    maxY = Math.max(maxY, f.bounds[3]);
  });
  var reserveY = 5;
  if (maxY < reserveY)
    maxY = reserveY;
  if (minY > -reserveY)
    minY = -reserveY;

  var theme = options.theme;

  var border = page.border;
  var pageWidth = page.width - 2 * border;
  var pageHeight = page.height - 2 * border;

  var visibleWidth = maxX - minX;
  var numPages = Math.ceil(visibleWidth / pageWidth);
  var centerX = numPages * pageWidth * -minX / visibleWidth;
  var centerY = pageHeight * -minY / (maxY - minY);

  var boxWidth = box.width * FT2MM * options.distance / options.height;

  var doc = new jsPDF(page.orientation, 'mm', page.format);

  for (var i = 1; i <= numPages; i++) {
    if (i > 1)
      doc.addPage();

    if (theme.backColor) {
      doc.setFillColor.apply(doc, theme.backColor);
      doc.rect(border, border, pageWidth, pageHeight, 'F');
    }

    doc.setDrawColor.apply(doc, theme.borderColor);
    doc.setLineWidth(theme.borderWidth);
    line(doc, 0, border, page.width, border);
    line(doc, 0, border + pageHeight, page.width, border + pageHeight);
    line(doc, border, 0, border, page.height);
    line(doc, border + pageWidth, 0, border + pageWidth, page.height);

    var offsetX = border + centerX - (i - 1) * pageWidth;
    var offsetY = border + centerY;

    doc.setDrawColor.apply(doc, theme.horizonColor);
    doc.setLineWidth(theme.horizonWidth);
    doc.lines([[pageWidth, 0]], border, offsetY, [1, 1], 'S');

    doc.setDrawColor.apply(doc, theme.centerColor);
    doc.setLineWidth(theme.centerWidth);
    doc.lines([[0, pageHeight]], offsetX, border, [1, 1], 'S');

    figures.forEach(function (f) {
      if (f.stop)
        doc.setFillColor.apply(doc, theme.stopPoleColor);
      else
        doc.setFillColor.apply(doc, theme.poleColor);

      doc.rect(offsetX + f.x - f.polew / 2, offsetY + f.y, f.polew, f.poleh, 'F');

      doc.setDrawColor.apply(doc, theme.outlineColor);
      doc.setLineWidth(theme.outlineWidth);
      doc.setFillColor.apply(doc, theme.plateColor);
      switch (f.type) {
        case "circle":
          doc.ellipse(offsetX + f.x, offsetY + f.y + f.rh, f.rw, f.rh, 'FD');
          break;
        case "rect":
          doc.rect(offsetX + f.x - f.w / 2, offsetY + f.y, f.w, f.h, 'FD');
          break;
      }
    });

    doc.setFillColor.apply(doc, theme.boxColor);
    doc.rect(offsetX - boxWidth / 2, border + pageHeight - theme.boxRaise,
      boxWidth, theme.boxRaise + border, 'F');

    doc.setFont(theme.font);
    doc.setFillColor.apply(doc, theme.textColor);
    doc.setFontSize(theme.textSize);
    if (i > 1)
      doc.text("" + (i - 1), 2, border - 2);
    if (i < numPages)
      doc.text("" + (i + 1), border + pageWidth + 2, border - 2);
    figures.forEach(function (f) {
      if (offsetX + f.x < border || offsetX + f.x > border + pageWidth) {
        return;
      }
      switch (f.type) {
        case "circle":
          var label = "R" + f.rw.toFixed(0) +
            (f.rw != f.rh ? "x" + f.rh.toFixed(0) : "");
          doc.text(label, offsetX + f.x, border - 2);
          break;
        case "rect":
          doc.text(f.w.toFixed(0) + "x" + f.h.toFixed(0), offsetX + f.x, border - 2);
          break;
      }
    });
    var heightMark = toFeet(options.height);
    var title = data.name +
      (data.boxes.length > 1 ? " (box " + (options.box + 1) + ")" : "") +
      " @" + heightMark + " W" + toFeet(options.distance) + " (p." + i + ")";
    doc.text(title, border + 2, border + pageHeight - 2 - Math.max(0, theme.boxRaise));
    doc.text(heightMark, 2, offsetY - 2);
    doc.text(heightMark, border + pageWidth + 2, offsetY - 2);
  }

  return doc.output('blob');
}
